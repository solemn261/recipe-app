import 'package:flutter/material.dart';
import 'package:recipe/recipe_card.dart';
import 'package:recipe/recipe_detail.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: <Widget>[
            Text("Hello, Ujwal!"),
            SizedBox(height: 12),
            Text(
              "What do you want to cook today?",
              style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 24),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                ),
                hintText: "Search recipe",
                fillColor: Colors.black12,
                filled: true,
                prefixIcon: Icon(Icons.search),
              ),
            ),
            SizedBox(height: 24),
            Wrap(
              alignment: WrapAlignment.spaceBetween,
              runSpacing: 16,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return RecipeDetail(
                        color: Colors.orangeAccent,
                        image: "assets/images/cake.png",
                        title: "Black Forest Cake",
                        views: 200,
                      );
                    }));
                  },
                  child: RecipeCard(
                    1,
                    image: "assets/images/cake.png",
                    title: "Black Forest",
                    color: Color(0XFFD3D57A),
                  ),
                ),
                RecipeCard(
                  2,
                  image: "assets/images/cake.png",
                  title: "Black Forest",
                  color: Color(0XFF7ACFD5),
                ),
                RecipeCard(
                  3,
                  image: "assets/images/cake.png",
                  title: "Black Forest",
                  color: Color(0XFFADD57A),
                ),
                RecipeCard(
                  4,
                  image: "assets/images/cake.png",
                  title: "Black Forest",
                  color: Color(0XFFD5A67A),
                ),
                RecipeCard(
                  5,
                  image: "assets/images/cake.png",
                  title: "Black Forest",
                  color: Color(0XFFD57A7A),
                ),
                RecipeCard(
                  6,
                  image: "assets/images/cake.png",
                  title: "Black Forest",
                  color: Color(0XFFD3D57A),
                ),
                RecipeCard(
                  7,
                  image: "assets/images/cake.png",
                  title: "Black Forest",
                  color: Color(0XFFD3D57A),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
