import 'package:flutter/material.dart';

class RecipeDetail extends StatelessWidget {
  final Color color;
  final String image;
  final String title;
  final int views;

  const RecipeDetail({Key key, this.color, this.image, this.title, this.views})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFeeeeee),
      body: ListView(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(24),
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(16),
                bottomRight: Radius.circular(16),
              ),
              boxShadow: [
                BoxShadow(
                  blurRadius: 8,
                  color: Colors.black26,
                  spreadRadius: 8,
                ),
              ],
            ),
            child: Column(
              children: <Widget>[
                Hero(
                  tag: "image1",
                  child: Image.asset(
                    image,
                    height: 200,
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        title,
                        style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(width: 16),
                    Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 6,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(24),
                      ),
                      child: Text(
                        "$views views",
                        style: TextStyle(
                            color: color, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              // 1
              Column(
                children: <Widget>[
                  Icon(Icons.timelapse, color: Colors.black38),
                  SizedBox(height: 4),
                  Text(
                    "45 min",
                    style: TextStyle(
                        color: Colors.black38, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              // 2
              Column(
                children: <Widget>[
                  Icon(Icons.people, color: Colors.black38),
                  SizedBox(height: 4),
                  Text(
                    "2 Servings",
                    style: TextStyle(
                        color: Colors.black38, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              // 3
              Column(
                children: <Widget>[
                  Icon(Icons.signal_cellular_4_bar, color: Colors.black38),
                  SizedBox(height: 4),
                  Text(
                    "Begineer",
                    style: TextStyle(
                        color: Colors.black38, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 24),
          Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 24),
                Text(
                  "Ingredients",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  children: <Widget>[
                    Image.asset(
                      image,
                      height: 32,
                      width: 32,
                      fit: BoxFit.contain,
                    ),
                    SizedBox(width: 12),
                    Image.asset(
                      image,
                      height: 32,
                      width: 32,
                      fit: BoxFit.contain,
                    ),
                    SizedBox(width: 12),
                    Image.asset(
                      image,
                      height: 32,
                      width: 32,
                      fit: BoxFit.contain,
                    ),
                    SizedBox(width: 12),
                    Image.asset(
                      image,
                      height: 32,
                      width: 32,
                      fit: BoxFit.contain,
                    ),
                  ],
                ),
                SizedBox(height: 24),
                Text(
                  "Description",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  """Step 1: Choose a Recipe. ...
Step 2: Choose the Right Baking Pans. ...
Step 3: Allow Ingredients to Reach Room Temperature. ...
Step 4: Prep the Pans. ...
Step 5: Preheat the Oven. ...
Step 6: Stir Together Dry Ingredients. ...
Step 7: Combine the butter and sugar.
Step 7: Combine the Butter and Sugar.""",
                ),
                SizedBox(height: 32),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
