import 'package:flutter/material.dart';

final List<Color> colors = [
  Color(0XFFD3D57A),
  Color(0XFF7ACFD5),
  Color(0XFFBD7AD5),
  Color(0XFFADD57A),
  Color(0XFFD57A7A),
  Color(0XFFD5A67A),
];
